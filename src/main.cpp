#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <Arduino.h>

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

#include <PubSubClient.h>
#include <SPI.h>

#include "DHT.h"

//#define DHTTYPE DHT11
#define DHTTYPE DHT22
#define DHTPIN  04

DHT dht(DHTPIN, DHTTYPE);

uint32_t delayMS;

IPAddress server(192, 168, 1, 22);

void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
}


WiFiClient espClient;
PubSubClient client(server, 1883, callback, espClient);

void publish(char* queue, float value) {
  char result[8];
  dtostrf(value, 6, 2, result);
  client.publish(queue, result);
  //Serial.printf("Value posted to queue '%s'\n", queue);
}


void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);

    WiFiManager wifiManager;
    wifiManager.autoConnect("AutoConnectAP");
    Serial.println("connected...yeey :)");

    Serial.println("DHTxx test!");

    dht.begin();

    if (client.connect("office", "user", "pass")) {
      client.publish("started","true");
      Serial.println("MQTT connected!!");
      //client.subscribe("office_status");
    } else {
      Serial.println("Holy crap!!! This MQTT shit isn't working!!!");
    }

}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C ");
  //Serial.print(f);
  //Serial.print(" *F\t");
  Serial.print("Heat index: ");
  Serial.print(hic);
  //Serial.print(" *C ");
  //Serial.print(hif);
  //Serial.println(" *F");
  Serial.println(";");

  publish("office/temperature", t);
  publish("office/humidity", h);
  publish("office/heatIndex", hic);
}
