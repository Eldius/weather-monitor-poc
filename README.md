# Poc ESP32 DHT11 #

## Circuit schematics ##

Image from Adafruit tutorial.
![png](docs/weather_dhtwiring.gif)

## ESP12E Pinout ##

Image from Filipeflop
![png](docs/Node-MCU-ESP-12E-Pin-Out-Diagram2.jpg)

## BOM ##

- 1 resistor 10k Ohms 5%
- 1 ESP8266
- 1 DHT (DHT11 or DHT22)

## Links ##

- [Adafruit - Using a DHTxx Sensor](https://learn.adafruit.com/dht/using-a-dhtxx-sensor)
- [Github Adafruit - DHTtester.ino](https://github.com/adafruit/DHT-sensor-library/blob/master/examples/DHTtester/DHTtester.ino)
- [Arduino Client for MQTTAPI Documentation](https://pubsubclient.knolleary.net/api.html)
- [IoT – Configurando um MQTT broker](https://www.dobitaobyte.com.br/iot-configurando-um-mqtt-broker/)
- [ESP32 MQTT – Publish and Subscribe with Arduino IDE](https://randomnerdtutorials.com/esp32-mqtt-publish-subscribe-arduino-ide/)
- [tzapu/WiFiManager](https://github.com/tzapu/WiFiManager)
- [](https://github.com/tzapu/WiFiManager/issues/241)
