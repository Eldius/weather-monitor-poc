
#include <BLEUtils.h>
#include <Arduino.h>

class MyServerCallbacks: public BLEServerCallbacks {
    //MyServerCallbacks() {};
    void onConnect(BLEServer* pServer) {
      // deviceConnected = true;
      Serial.println("Device connected...");
    };

    void onDisconnect(BLEServer* pServer) {
      // deviceConnected = false;
      Serial.println("Device disconnected...");
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    //MyCallbacks() {};
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        Serial.println("*********");
        Serial.print("Command: ");
        for (int i = 0; i < rxValue.length(); i++)
          Serial.print(rxValue[i]);
        Serial.println();
        Serial.println("*********");
      }
    }
};
